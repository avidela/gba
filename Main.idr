module Main

import Decidable.Equality
import Data.Maybe
import Data.Fin
import Data.Vect
import Data.DPair
import Data.Nat 

import Control.Monad.State

%default total

infixr 0 %->
infixr 5 %%

data Affine : Bool -> Type -> Type where
  NotConsumed : (0 _ : t) -> Affine False t
  Consumed : (1 _ : t) -> Affine True t

record RTAffine (t : Type ) where
  constructor MkRT
  1 isConsumed : Bool
  1 val : Affine isConsumed t

data (%->) : Type -> Type -> Type where
  MkAffine : (1 f : (1 _ : RTAffine a) -> b) -> (a %-> b)

||| First argument is linear, second is dependent and erased
record LEPair (t : Type) (s : t -> Type) where
  constructor (%%)
  1 fst : t
  0 snd : s fst

Word : Type
Word = Fin 16

Word_eq : (w, w' : Word) -> Dec (w = w')
Word_eq = decEq

Null : Word
Null = 0

fSucc : {a : _} -> Fin a -> Maybe (Fin a)
fSucc x with (strengthen (FS x))
  fSucc x | (Left y) = Nothing
  fSucc x | (Right y) = ?fSucc_rhs_2

||| Addition mod `a`
||| prim_add_int32 
modSum : {a : _} -> Fin a -> Fin a -> Fin a
modSum FZ y = y
modSum (FS x) y with (fSucc y)
  modSum (FS x) y | Nothing = weaken x
  modSum (FS x) y | (Just z) = assert_total (modSum (weaken x) z)


next : Word -> Word
next = modSum 1

Store : Nat -> Type -> Type
Store n = State (Vect n (Fin n))

WStore : Type -> Type
WStore = Store 16

||| int index(int i, int[32KB] mem)
index : Fin n -> Vect n a -> a
index FZ (x :: xs) = x
index (FS x) (y :: xs) = Main.index x xs

set : Fin n -> a -> Vect n a -> Vect n a
set FZ y (x :: xs) = y :: xs
set (FS x) y (z :: xs) = z :: set x y xs


data PtsTo : (mem : Vect n a) -> (loc : Fin n) -> (x : a) -> Type where
  Here : PtsTo (x :: xs) FZ x
  There : PtsTo xs l' x -> PtsTo (z :: xs) (FS l') x

data LSeg : (start : Word) -> (end : Word) -> (mem : List Word) -> Type where
  End  : LSeg x x []
  Step : (0 e : (x = y) -> Void) ->
         (1 p1 : PtsTo x w) ->
         (1 p2 : PtsTo (next x) x') ->
         (1 p3 : LSeg x' y xs) ->
         LSeg x y (w :: xs)

data AllNullPointers : (mem : Vect m (Fin n)) -> m `LTE` n -> Type where
  MemEmpty : AllNullPointers [] LTEZero
  MemCons : {mem : Vect m (Fin (S n))} -> 
            {auto lte : m `LTE` S n} ->
            PtsTo (FZ :: mem) FZ FZ -> 
            AllNullPointers mem lte ->  
            AllNullPointers (FZ :: mem) ?wot

{-
-- fancyAppend : Vect k (Fin k) -> Vect n (Fin n) -> Vect (k + n) (Fin (k + n))
-- fancyAppend xs ys = map (weakenN _) xs ++ map (weakenN' k) ys
--   where
--     weakenN' : (0 n : Nat) -> Fin m -> Fin (n + m)
--     weakenN' n x = rewrite plusCommutative n m in weakenN n x
-- 
-- data AllNullPointers : (mem : Vect n (Fin n)) -> Type where 
--   MemEmpty : AllNullPointers [] 
--   MemCons : {locs : Vect k (Fin k)} -> 
--              PtsTo mem loc FZ -> 
--             AllNullPointers mem -> 
--             AllNullPointers (fancyAppend locs mem) 

initialMemory : (n : Nat) -> (LEPair (Vect n (Fin n)) AllNullPointers )
initialMemory 0 = [] %% MemEmpty
initialMemory (S k) = let (mem' %% pts) = initialMemory k in 
                          FZ :: (mem') %% MemCons Here ?constPtr

record WordStorage {0 n : Nat} {0 a : Type} (memory : Vect n a) (loc : Fin n) (w : a) where
  constructor MkWordStorage
  word : a
  0 same : word = w
  0 ptr : PtsTo memory loc word

read : (1 mem : Vect n a) => (1 loc : Fin n) -> (0 w : a) -> (0 p : PtsTo mem loc w) ->
       (WordStorage mem loc w)
read {mem = w :: ms} FZ w Here = MkWordStorage w Refl Here
read {mem = m :: ms} (FS l') w (There x) = 
  let rec = read {mem=ms} l' w x in 
      MkWordStorage rec.word rec.same (There rec.ptr)

data LVect : Nat -> Type -> Type where
  Nil : LVect 0 a
  (::) : (1 _ : a) -> (1 _ : LVect n a) -> LVect (S n) a

write : (1 mem : Vect l a) => (1 loc : Fin l) -> (overwrite : a) ->
        (Subset (Vect l a) (\m => PtsTo m loc overwrite))
write {mem = m :: ms} FZ overwrite = Element (overwrite :: ms ) Here
write {mem = m :: ms} (FS x) overwrite = 
  let (Element ms' ptr) = write {mem=ms} x overwrite in 
      Element (m :: ms') (There ptr)


smallMemory : (LEPair (Vect 4 (Fin 4)) AllNullPointers)
smallMemory = initialMemory 4

index' : {mem : Vect (S n) (Fin (S n))} -> (i : Fin (S n)) -> AllNullPointers mem -> PtsTo mem i FZ
index' FZ (MemCons x y) = ?index'Sd
index' (FS y) x = ?index'_rhs_2

program : (LEPair (Vect 4 (Fin 4)) AllNullPointers) -> Vect 4 (Fin 4)
program (mem %% ptrs) =
  let 0 p1 = index' 0 ptrs in
  -- let mem2 = write ?program_rhs 3
  ?rest

{-

data LSeg : (x : Word) -> (y : Word) -> (ws : List Word) -> Type where
  End  : LSeg x x []
  Step : (0 e : (x = y) -> Void) ->
         (1 p1 : PtsTo x w) ->
         (1 p2 : PtsTo (next x) x') ->
         (1 p3 : LSeg x' y xs) ->
         LSeg x y (w :: xs)
{-

data DLPair : (A : Type) -> (B : A -> Type) -> Type where
  MkDLPair : {A : Type} -> {B : A -> Type} -> (1 a : A) -> (1 b : B a) -> DLPair A B

trev : List a -> List a -> List a
trev []        ys = ys
trev (x :: xs) ys = trev xs (x :: ys)

reverse : List a -> List a
reverse xs = trev xs []

voidL : (0 q : Void) -> (1 x : a) -> b
voidL _ impossible

mutual
  revInner : (y, z : Word) ->
        (0 ws : List Word) ->
        (0 ws' : List Word) ->
        (1 l1 : LSeg y Null ws) ->
        (1 l2 : LSeg z Null ws') ->
        DLPair Word (\x => LSeg x Null (trev ws ws'))
  revInner y z ws ws' l1 l2 = help y z ws ws' (Word_eq y Null) l1 l2

  help : (y : Word) ->
         (z : Word) ->
         (0 ws : List Word) ->
         (0 ws' : List Word) ->
         (d : Dec (y = Null)) ->
         (1 l1 : LSeg y Null ws) ->
         (1 l2 : LSeg z Null ws') ->
         DLPair Word (\x => LSeg x Null (trev ws ws'))
  help _ z _ _ (Yes Refl) End l2 =
    MkDLPair z l2
  help _ z _ _ (Yes Refl) (Step q p1 p2 p3) l2 =
    voidL (q Refl) (l2, p1, p2, p3)
  help y z (w :: xs) ws' (No p) (Step {x'} {w} {xs} q p1 p2 p3) l2 =
    revInner x' y xs (w :: ws') p3 (Step q p1 (write (next y) x' z p2) l2)

rev : (   x : Word) ->
      (0 ws : List Word) ->
      (1 l  : LSeg x Null ws) ->
      DLPair Word (\z => LSeg z Null (Linear.reverse ws))
rev x ws l = revInner x Null ws [] l End

-}
